var indexSectionsWithContent =
{
  0: "abcdefghilmorstuvwxy~",
  1: "bcdgmsu",
  2: "egmu",
  3: "dfghilmostu~",
  4: "abcdlmrstvwxy",
  5: "dt",
  6: "als"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Pages"
};


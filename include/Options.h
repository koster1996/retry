/// Header file for the GameOptions class
/// Author: Jeroen Minnema & Bas Koster
#ifndef GAME_OPTIONS
#define GAME_OPTIONS
#include <fstream>
#include <iostream>
#include <sstream>

/// Enum consisting of the three levels which could possibly be selected
enum GAME_LEVEL { GAME_LEVEL_ONE, GAME_LEVEL_TWO, GAME_LEVEL_THREE };

/// Enum representing the three possible difficulties (horizontal flight speeds)
enum class GAME_DIFFICULTY {
    GAME_DIFFICULTY_EASY,
    GAME_DIFFICULTY_MED,
    GAME_DIFFICULTY_HARD
};

/// Planes which can be selected in PlaneMenu, represent a specific icon and
/// possibly flight characteristics.
enum PLANE_TYPE { PLANE_TYPE_1, PLANE_TYPE_2, PLANE_TYPE_3 };

/// Class defining what the options are for loading a given level.
/// Defines a difficulty and level to load actually. Will be important when the
/// game has different levels. Consisting mainly of setter and getter functions
/// for the GAME_LEVEL, GAME_DIFFICULTY and PLANE_TYPE enums.
class GameOptions
{
  public:
    GameOptions(); /// Initialize options with level 1, medium difficulty and
                   /// plane 2

    // Setter functions
    void setDifficulty(GAME_DIFFICULTY diff) { difficulty = diff; }
    void setLevel(GAME_LEVEL lev) { level = lev; }

    // Getter functions
    GAME_LEVEL getLevel() { return level; } 
    PLANE_TYPE getPlaneType() { return plane; };
    void setPlaneType(PLANE_TYPE input) { plane = input; };

  private:
    GAME_LEVEL level;
    GAME_DIFFICULTY difficulty;
    PLANE_TYPE plane;

   // int testLevel = 2; // This is a test variable
};

#endif // !GAME_OPTIONS

/// Header file for the plane menu
/// Author: Jeroen Minnema
#ifndef SRC_HIGHSCOREMENU
#define SRC_HIGHSCOREMENU

#include <SDL2/SDL.h>
#include "Options.h"
#include <stdio.h>
#include <KeyPress.h>
#include "MenuPage.h"
#include "Highscores.h"

/// HighscoreMenu class, inheriting from MenuPage and extending the functionality to 
/// allow printing the highscores (by creating an instantiation of the HighScores object
/// an printing.
class HighscoreMenu : public MenuPage
{
  public: //Public functions
    HighscoreMenu(SDL_Window *window, SDL_Surface *screenSurface,
                  SDL_Renderer *renderer); // Constructor

    virtual KeyPress getUserInput() override;         // get user input
    virtual bool handleInput(KeyPress) override; // handle input

  private: // Private variables, involved with SDL library
    std::string texturePath;

};

#endif

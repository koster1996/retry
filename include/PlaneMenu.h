/// Header file for the plane menu
/// Author: Jeroen Minnema
#ifndef SRC_PLANEMENU
#define SRC_PLANEMENU

#include <SDL2/SDL.h>
#include "Options.h"
#include <stdio.h>
#include <KeyPress.h>
#include "MenuPage.h"

/// PlaneMenu class, inheriting from GameStage and extending the functionality to render a window and load the
/// Desired textures.
/// Operates very similar to the main menu, so in the final iteration 
/// this will probably be reworked to one menu page class on which can be extended as desired
class PlaneMenu : public MenuPage
{
  public: //Public functions
    PlaneMenu(SDL_Window *window, SDL_Surface *screenSurface,
			  SDL_Renderer *renderer, GameOptions *options); // Constructor
  
    virtual KeyPress getUserInput() override;         // get user input
    virtual bool handleInput(KeyPress input) override; // handle input

  private: // Private variables, involved with SDL library
    std::string texturePath;
    GameOptions *gameOptions;
    bool hasChosen;

};

#endif

/// Header file for the menu class
/// Author Jeroen Minnema

#include "MenuPage.h"
#include "Options.h"
#include <KeyPress.h>
#include <SDL2/SDL.h>
#include <stdio.h>

#ifndef SRC_MENU
#define SRC_MENU

/// MainMenu class inheriting from MenuPage being one of the window menus. Class
/// renders initial window and handles input which allows setting options, showing highscore and plane menu pages
/// as well as starting the game.
class mainMenu : public MenuPage
{
  public: // Public functions
    mainMenu(SDL_Window *window, SDL_Surface *screenSurface,
             SDL_Renderer *renderer,
             GameOptions *options); //! Constructor initializing a window


    virtual KeyPress getUserInput() override; // get User input
    virtual bool handleInput(KeyPress) override;

  private: // Private variables, involved with SDL library
    std::string texturePath;
    GameOptions *gameOptions;
};

#endif
//#pragma once

#include <SDL2/SDL.h>
// Retry project
// Main file
#include "Game.h"
#include "MainMenu.h"
#include "HighscoreMenu.h"
#include "GameStage.h"
#include "Options.h"
#include <KeyPress.h>
#include <vector>

#ifndef SRC_GAMEMANAGER
#define SRC_GAMEMANAGER

///
/// Class responsible for handling the different GameStages
class GameManager
{

  public:
    GameManager();
    ~GameManager();

  private: // Private variables
    SDL_Renderer *renderer = NULL;
    SDL_Texture *texture = NULL;
    SDL_Window *window = NULL;
    SDL_Surface *screenSurface = NULL;

    GameOptions *options;
    GameStage *gameStage = NULL;

    const int SCREEN_WIDTH = 1280;
    const int SCREEN_HEIGHT = 960;

    const Uint32 delay = 1000 / 60;
    SDL_TimerID timer_id;

    bool run_program;

    void mainloop();

    void handleInput(KeyPress);
};

#endif

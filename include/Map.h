/// Header file for Map
/// Author: Michiel Richter

#ifndef MAP
#define MAP

#pragma once

#include "GameObject.h"
#include "Options.h"
#include <vector>
#include <memory>

/**
Map is called by the game to initialize the playing environment and generate all
objects within the environment relevant to the game. A public function can be
called to return the vector of GameObjects stored in the vector. In addition,
the Map contains the dimensions of the game. Furthermore, a templated function
is used to generate all GameObjects, able to generate a PLANE, COIN, and BOUNDS
of the map.
*/

/// Nameless enumerate class containing the tilesize in pixels, and numbers in
/// the map associated with different GameObjects: 2 = PLANE, 3 = COIN
enum ObjectNums {
    TILESIZE = 64,
    OBJECTSIZE = 32,
    BACKGROUND_NUM = 1,
    WALL_NUM = 0,
    PLANE_NUM = 2,
    COIN_NUM = 3,
	DIRT_NUM = 4,
	GRASS_NUM = 5,
	SKYGRASS_NUM = 6,
	FINISHLINE_NUM = 9
};

/// Initialises the playing environment and generates all objects 
/// within the environment.
class Map
{
  public:
    Map(GameOptions *options);

    /// \return vector of GameObject pointers
    std::vector<std::shared_ptr<GameObject>> getMapObjects()
    {
        return objectsVector;
    };

    std::vector<std::vector<int>> getLevel() { return level; };

	std::vector<ObjectNums> getObjectNumbers() { return std::move(objectNumbers); };
    std::vector<Type> getObjectTypes() { return std::move(objectTypes); };

	int getPlaneSSNumber() { return ss_number; };
  private:
    int ss_number;
    std::vector<std::shared_ptr<GameObject>> objectsVector;
    std::vector<std::vector<int>> map;

	// Vector of all object numbers
    std::vector<ObjectNums> objectNumbers = {WALL_NUM,
                                             PLANE_NUM, COIN_NUM, DIRT_NUM, GRASS_NUM, SKYGRASS_NUM, FINISHLINE_NUM};

	// Vector of all object types
    std::vector<Type> objectTypes = {WALL, PLANE, COIN, DIRT, GRASS, SKYGRASS, FINISHLINE};

    int MAP_HEIGHT;
    int MAP_WIDTH;

    /// Loops through the map and calls to createObject to make a GameObject at
    /// the relevant position
    void makeLevelObjects(std::vector<std::vector<int>> &map,
                          GameOptions *options);

    /// Loads the map based on selected level and difficulty
    void setLevel(GameOptions *options);

    /// Template function. Creates new GameObject and adds it to vector.
    /// typename T specifies type of GameObject
    template <typename T>
    void createObject(std::vector<double> xyposition, double height,
                      double length, double orientation, T type)
    {
        std::shared_ptr<GameObject> p(
            new GameObject(xyposition, height, length, orientation, type));
        objectsVector.push_back(std::move(p));
    }

    void createPlaneObject(std::vector<double> xyposition, double height,
                           double length, double orientation, PLANE_TYPE type);

    void createCoinObject(std::vector<double> xyposition, double height,
                           double length, double orientation);


    /// 2D vector containing the level
    std::vector<std::vector<int>> level;
};

#endif

/// GameObject.h
/// Header file declaring all of the in-game objects such as planes, clouds, and obstacles
/// Author: Christophe van der Walt Created: 27 November 2018

#ifndef GAMEOBJECT
#define GAMEOBJECT
#include "Options.h"
#include <memory>
#include <vector>

///
/// Enum for giving our objects a type.
enum Type { PLANE, COIN, WALL, BOUNDS, DIRT, GRASS, SKYGRASS, FINISHLINE };

/// Base game object. Has a position, dimensions and an orientation. They all
/// occupy rectangular regions of space. Non member methods include checking
/// collision between itself and another GameObject, and fetching the corners of
/// the rectangle.
class GameObject
{
  public:
    GameObject(const std::vector<double>, const double, const double,
               const double, const Type);

    void setPosition(std::vector<double>);
    void setOrientation(double);
    void setHeight(double);
    void setLength(double);

    std::vector<double> getPosition();
    double getOrientation();
    double getHeight();
    double getLength();
    std::vector<std::vector<double>> getCorners();
    double getSideScrollVel();

    virtual bool hasCollided(std::shared_ptr<GameObject>);
    virtual void updatePosition(double, bool, double);
    virtual void setDeath(bool);
    virtual bool isDead();
    virtual int getValue();
    virtual void setValue();

    Type getType();
    void setType(Type);

  private:
    std::vector<double> position;
    double orientation;
    double height;
    double length;
    double side_scroll_vel;
    Type type;
};

/// Derived from GameObject
/// Adds functionality for plane dynamics and different types
class Plane : public GameObject
{
  public:
    Plane(const std::vector<double>, const double, const double, const double,
          const Type, const PLANE_TYPE);

    virtual void setDeath(bool) ;
    virtual bool isDead() ;
    virtual void updatePosition(double, bool, double);
    void setType(PLANE_TYPE);

  private:
    bool death; // true if dead, false if not
    double gravity;
    double upVel;
    PLANE_TYPE plane;
};

/// Coin object derived from DynamicObject.
/// Adds functionality for being able to have a dynamically changeable value
/// associated to it.
class Coin : public GameObject
{
  public:
    Coin(const std::vector<double>, const double, const double, const double,
         const Type, const int);

    virtual void setValue(int);
    virtual int getValue();

    virtual void setDeath(bool);
    virtual bool isDead();

  private:
    int value;
    bool death;
};

/// Defines the boundaries of the game field
/// It is represennted in this manner as we need a collision method on such a
/// thing.
class Bounds : public GameObject
{
  public:
    Bounds(const std::vector<double>, const double, const double, const double,
           const Type);
    //~Bounds();

    virtual bool hasCollided(std::shared_ptr<GameObject>); // const override;
};

#endif // GAMEOBJECT
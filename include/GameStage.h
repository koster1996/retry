/// GameStage.h
/// Header file for abstract for menu and game class, currently
/// Author: Bas Koster 10-12-2018

#include <KeyPress.h>

#ifndef GAMESTAGE
#define GAMESTAGE

///
/// Abstract class from which Game and MenuPage are derived
class GameStage
{
  public:
    GameStage();
    virtual ~GameStage() = default;	
	virtual void render() const;
    virtual KeyPress getUserInput();
    virtual void update();
    virtual bool handleInput(enum KeyPress);
};

#endif // !GAMESTAGE


//#pragma once
///Header file for the general game class
///Author: Bas Koster


#include <SDL2/SDL.h>
#include <stdio.h>
#include <vector>
#include "Map.h"
#include "GameObject.h"
#include "Highscores.h"
#include "GameStage.h"
#include "KeyPress.h"

#ifndef SRC_GAME
#define SRC_GAME

/// Base class game. Takes SDL window, surface and renderer as input, and generates the view for the game.
/// GameObject is a member of game. 
class Game : public GameStage
{
  public:
    Game(SDL_Window *window, SDL_Surface *screenSurface, SDL_Renderer *renderer,
         GameOptions *options);
    virtual ~Game();
    virtual void render() const override; //render menu on screen
    virtual void update() override;
    virtual KeyPress getUserInput() override;
    virtual bool handleInput(KeyPress) override;

  private:
    SDL_Window *window;         //    = NULL; // The surface
                                // contained by the window
    SDL_Surface *screenSurface; //= NULL;
    SDL_Renderer *renderer;

	SDL_Texture *textures;
    SDL_Event e;

	Map *map = NULL;

	bool keyPressed;
	std::vector<std::shared_ptr<GameObject>> GameObjects;

	std::vector<ObjectNums> objectNumbers;
    std::vector<Type> objectTypes;

	bool isDead;

    double time;
    double last_press_time;
    double delta_t;
    double clock;
    double start_of_game;

	int score;
	int plane_ss_number;

	void loadTextures();
    std::vector<std::vector<int>> level;
};

#endif
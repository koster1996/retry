/// Header file for the highscores class and scoreEntry struct
/// Author: Jeroen Minnema
#ifndef GAME_SCORES
#define GAME_SCORES
#include <sstream>
#include <iostream>
#include <fstream>
#include <vector>

/// scoreEntry represents a single high score entry, storing a name and score, goes along with its constructor for 
/// convenient initialization.
struct scoreEntry {
    std::string name;
    double score;
    scoreEntry(std::string, double);
    void print();
};


/// Class defining the highscores of the game, including functionality to load or save levels.
/// Uses prompt output as the default SDL library does not support text printing on screen
/// without adding an additional 'true typefont'library. Therefore CMD prompt has been used.
/// Public methods allow adding new entries, printing all scores.
class HighScores
{
  public:
    HighScores(); /// Reads all scores present in the textfile by calling loadHighscores()
    void newEntry(double score); /// Create new entry, 
    void showHighscores();
    

  private:
    void saveEntry(
        scoreEntry entry); /// Write new entry to textfile and push to data vector
    void
    loadHighscores(); /// Open highscores.txt located in resources directory and
                      /// use ifstream to read it line by line
   
    std::vector<scoreEntry> data; /// Vector consisting of all scores currently know to the program, loaded on program start and new 
	/// scores are loaded to this vector.
};


#endif // !GAME_SCORES

/// Header file for the MenuPage class
/// Author: Jeroen Minnema
#ifndef SRC_MENUPAGE
#define SRC_MENUPAGE

#include <SDL2/SDL.h>
#include "Options.h"
#include <stdio.h>
#include "GameStage.h"

/// Menu page class inheriting from gamestage, being one of the possible stages. Extends on GameStage by adding menu specific functionality. 
/// PlaneMenu, MainMenu and HighscoresMenu are derived from this and add page specific functionality.
class MenuPage : public GameStage
{
  public: // Public functions
<<<<<<< HEAD
    MenuPage(SDL_Window *window, SDL_Surface *screenSurface,
             SDL_Renderer *renderer); //! Constructor initializing a window
    virtual ~MenuPage();              // Destructor
=======
   MenuPage(SDL_Window *window, SDL_Surface *screenSurface, SDL_Renderer *renderer); //! Constructor initializing a window
    ~MenuPage();
>>>>>>> 2d85e329dd16cf556ec8ca9af7a75b84dc46253c

    virtual void render() const override;     //! Render menu on screen
   // virtual KeyPress getUserInput() override; // get User input
    //virtual bool handleInput(KeyPress) override;

  protected: // Private variables, involved with SDL library
    SDL_Window *window;
    SDL_Surface *screenSurface;
    SDL_Renderer *renderer;

    SDL_Texture *loadTextures(std::string path);
    SDL_Texture *menuPageTexture; // Contains the main menu texture
    SDL_Event e;                  // Used for handling user input
    std::string getTexturePath() { return texturePath; }
	std::string texturePath;
};


#endif

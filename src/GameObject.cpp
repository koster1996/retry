/// GameObject.cpp
/// Defining all of the in-game objects such as planes, clouds, and obstacles
/// Author: Christophe van der Walt
/// Created: 27 November 2018

#define M_PI1 3.141592653589793238462643383279502884L /* pi */
#include "GameObject.h"
#include "math.h"
#include <iostream>
#include <vector>

/// Constructor for the game object class. All objects, mechanically speaking,
/// are rectangular. Takes in a position vector, a height (refers to the
/// y-dimension at w=0), a length (refers to the x-dimension at w=0), an
/// orientation, and a type so we can distinguish between planes and other
/// objects as we may want to call different methods on them.
GameObject::GameObject(const std::vector<double> xyposition,
                       const double hheight, const double llength,
                       const double worientation, const Type ttype)
{
    setPosition(xyposition);
    setHeight(hheight);
    setLength(llength);
    setOrientation(worientation);
    setType(ttype);
    side_scroll_vel = 50;
}

///
/// Sets the position of the top-right corner of the object (if orientation is
/// 0).
void GameObject::setPosition(std::vector<double> xyposition)
{
    position = xyposition;
}

///
/// Rotates the object about the point for which we fix its position by an
/// inputted amount of radians.
void GameObject::setOrientation(double worientation)
{
    orientation = worientation;
}

///
/// Sets the height of the object.
void GameObject::setHeight(double hheight) { height = hheight; }

///
/// Sets the length of the object.
void GameObject::setLength(double llength) { length = llength; }

///
/// Returns the current orientation of the object as a double.
double GameObject::getOrientation() { return orientation; }

///
/// Returns the height of the object as a double.
double GameObject::getHeight() { return height; }

///
/// Returns the length of the object as a double.
double GameObject::getLength() { return length; }

double GameObject::getSideScrollVel() { return side_scroll_vel; }

///
/// Returns the type of the object.
Type GameObject::getType() { return type; }
///
/// Allows us to change the type of the object dynamically (if we would
/// want to in the future).
void GameObject::setType(Type input) { type = input; }

///
/// Returns the position of the object as a vector of doubles.
std::vector<double> GameObject::getPosition() { return position; }

///
/// Uses simple trigonometry to return a vector of corner coordinates (also
/// expressed as vectors of doubles).
std::vector<std::vector<double>> GameObject::getCorners()
{
    std::vector<std::vector<double>> corners{{0, 0}, {0, 0}, {0, 0}, {0, 0}};

    // Position of corner A
    corners.at(0).at(0) = position.at(0);
    corners.at(0).at(1) = position.at(1);

    // Position of corner B
    corners.at(1).at(0) = position.at(0) + height * sin(orientation);
    corners.at(1).at(1) = position.at(1) + height * cos(orientation);

    // Position of corner C
    corners.at(2).at(0) = corners.at(3).at(0) + length * cos(orientation);
    corners.at(2).at(1) = corners.at(3).at(1) - length * sin(orientation);

    // Position of corner D
    corners.at(3).at(0) = position.at(0) + length * cos(orientation);
    corners.at(3).at(1) = position.at(1) - length * sin(orientation);

    return corners;
}

/// Checks to see if two objects have collided.
/// Simply checks distance between two corners. This is a bad algorithm, but
/// this course isn't about algorithms so it doesn't matter.
bool GameObject::hasCollided(std::shared_ptr<GameObject> other_object)
{
    bool hasCollided = false;

    double collidion_distance = 40;

    std::vector<double> pose = {0, 0};
    pose.at(0) = getPosition().at(0) - other_object->getPosition().at(0);
    pose.at(1) = getPosition().at(1) - other_object->getPosition().at(1);

    double distance = sqrt(pose.at(0) * pose.at(0) + pose.at(1) * pose.at(1));

    if (distance > collidion_distance) {
        hasCollided = false;
    } else {
        hasCollided = true;
    }

    return hasCollided;
}

/// Default for virtual function updatePosition();
/// Moves objects to the left at a given velocity;
void GameObject::updatePosition(double time_since_last_keypress,
                                bool keyPressed, double delta_t)
{
    //updatePosition is virtual, so we can't change the argument list, but some parameters are unused and so throw warnings. So,
    //we're just using the arguments in a dummy manner to remove the warnings. This is bad practice, but you guys wanted no warnings.
    time_since_last_keypress = 1;
    keyPressed = 1;
    std::vector<double> new_position = getPosition();
    if (keyPressed){
        new_position.at(0) = time_since_last_keypress*new_position.at(0) - (delta_t)*getSideScrollVel();
    }
    setPosition(new_position);
}

/// Function for setting the death parameter of the plane.
/// This needs to be a member of GameObject because of other code. This is bad
/// design, this should not exist. The plane can always be isolated from the
/// rest of the code.
void GameObject::setDeath(bool) {}

/// Function for checking if the plane is dead.
/// This should not exist. See comments above.
bool GameObject::isDead() { return false; }

int GameObject::getValue() { return 0; }

void GameObject::setValue() {}

///
/// Free helper function for getting the sign of a double. Used in
/// hasCollided(). Not anymore lol.
int sign(double a)
{
    if (a > 0)
        return 1;
    if (a < 0)
        return -1;
    return 0;
}

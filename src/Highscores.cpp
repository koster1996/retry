#include "Highscores.h"

///
/// Constructor HighScores object
HighScores::HighScores() { loadHighscores(); }

///
/// Asks use to enter new high score via CMD prompt. Asks use to enter his name, a new entry is made using the provided score and name.
void HighScores::newEntry(double score)
{
	// Get score from game.
    std::cout << "Congrats! You've scored "<< score << " point! \n Would you like to add your last attempt to the highscores? y/n \n";
    std::string inp;
    std::cin >> inp;

    if (inp == "y") {
		//Enter new score:
        std::cout << "Adding your last attempt to the highscores, please enter your "
                "name: \n";
        std::cin >> inp;
        if (!inp.empty()) { // Check for empty name input

			std::cout << "Adding: "<< inp << "with score:" << score
                 << " to the highscores, please play again! \n";
            saveEntry(scoreEntry(inp, score));
			}
            else
            {
                std::cout << "Something went wrong, please try to add your score "
                        "again";
                newEntry(score);
            }


        
    }
}

///
/// Prints all stored high-scores
void HighScores::showHighscores() {
    std::cout << "Printing highscores: \n";
    for (auto i : data) // access by value, the type of i is int
        i.print();
}

/// saveEntry() takes a scoreEntry struct object as an input argument and writes
/// it's name and score to 'highscores.txt' located in recources.
void HighScores::saveEntry(scoreEntry entry)
{
    std::ofstream scoreFile("./resources/highscores.txt");
    data.push_back(entry);
    if (scoreFile.is_open()) {
        for (unsigned int i = 0; i < data.size(); i++) {
            scoreFile << "Name: \t" << data[i].name << " \t score: \t" 
                      << data[i].score << "\n";
           
        }
        scoreFile.close();
        
    } else
        std::cout << "Unable to open file";
}

///
/// reads text file and loads every entry to data vector.
void HighScores::loadHighscores(){
    std::ifstream file("./resources/highscores.txt");
    std::string str;
    double score;
    std::string name;
    std::string buff;
	/// Read every file line
    while (std::getline(file, str)) {
        std::istringstream iss(str);
        // string word;
        iss >> buff;
        iss >> name;
        iss >> buff;
        iss >> score;
        data.push_back(scoreEntry(name, score));
    }
}

///
/// New entry using score constructor.
scoreEntry::scoreEntry(std::string nam, double scr)
{
    name = nam;
    score = scr;
}

///
/// Prints an individual entry in the high-scores file
void scoreEntry::print() {
    std::cout << "Name: " << name << "\t" << "Score:  " << score << "\n";

}

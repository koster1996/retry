#define M_PI1 3.141592653589793238462643383279502884L /* pi */
#include "GameObject.h"
#include "math.h"
#include <iostream>
#include <vector>

/// Constructor for DynamicObject
/// Adds previous position and velociy properties to the standard object class.
/// Assigns physical properties to the plane based on their type.
Plane::Plane(const std::vector<double> pposition, const double worientation,
             const double llength, const double hheight, const Type ttype,
             const PLANE_TYPE ptype)
    : GameObject(pposition, hheight, llength, worientation, ttype)
{
    setType(ptype);
    if (ptype == PLANE_TYPE_1){
        gravity = 2;
        upVel = 1;
    }
    else if (ptype == PLANE_TYPE_2){
        gravity = 2.01;
        upVel = 1.01;
    }
    else {
        gravity = 2.02;
        upVel = 1.02;
    }
    setDeath(false);
}

///
/// Function for setting the death status of the plane.
void Plane::setDeath(bool isDead) { death = isDead; }

///
/// Function for checking the deeath status of the plane.
bool Plane::isDead() { return death; }

///
/// Function for setting the type of the plane.
void Plane::setType(PLANE_TYPE plane_type) { plane = plane_type; }

/// Overrides updatePosition in base class GameObjects
/// Makes the object move in the y-direction based on key press
void Plane::updatePosition(double time_since_last_keypress, bool keyPressed,
                           double delta_t)
{
    //updatePosition is virtual, so we can't change the argument list, but some parameters are unused and so throw warnings. So,
    //we're just using the arguments in a dummy manner to remove the warnings. This is bad practice, but you guys wanted no warnings.
    delta_t = 1;
    std::vector<double> initial_position = getPosition();
    double initial_orientation = getOrientation();

    std::vector<double> new_position = initial_position;
    double new_orientation = initial_orientation;

    if (keyPressed) {
        new_position.at(1) =
            initial_position.at(1) - upVel * time_since_last_keypress;
        new_orientation = atan2(upVel, getSideScrollVel());

    } else {
        new_position.at(1) =
            initial_position.at(1) - upVel * time_since_last_keypress +
            0.5 * gravity * time_since_last_keypress * time_since_last_keypress * delta_t;
        new_orientation =
            atan2(upVel - 0.5 * gravity * time_since_last_keypress,
                  getSideScrollVel());
    }
    setPosition(new_position);
    setOrientation(180 * new_orientation / 3.141592);
}

#define M_PI1 3.141592653589793238462643383279502884L /* pi */
#include "GameObject.h"
#include "math.h"
#include <iostream>
#include <vector>

// ---------------------------------COIN-----------------------------------------
/// Constructor for the Coin class.
/// Adds the ability to have a value.
Coin::Coin(const std::vector<double> pose, const double worientation,
           const double llength, const double hheight, const Type ttype,
           const int val)
    : GameObject(pose, worientation, llength, hheight, ttype)
{
    setValue(val);
    setDeath(false);
}

///
/// Function for setting the death status of the coin.
void Coin::setDeath(bool isDead) { death = isDead; }

///
/// Function for checking the deeath status of the coin.
bool Coin::isDead() { return death; }

///
/// Sets the value for the coin (in case it needs to be adjusted dynamically).
void Coin::setValue(int val) { value = val; }

///
/// Returns the value as an integer.
int Coin::getValue() { return value; }


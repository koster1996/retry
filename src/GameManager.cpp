#include "GameManager.h"
#include "Game.h"
#include "GameStage.h"
#include "MainMenu.h"
#include "PlaneMenu.h"
#include <iostream>

/**
Constructor. Creates an SDL window, associate an SDL renderer with the window,
and associate an SDL surface with the window.
*/
GameManager::GameManager()
{
	// Set the condition for running the program
	run_program = true;

	// Create an SDL window upon startup
	window = SDL_CreateWindow("Retry: The game", SDL_WINDOWPOS_UNDEFINED,
							  SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH,
							  SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
	if (window) {
		std::cout << "Window created successfully" << std::endl;
	}

	// Create a window renderer upon startup
	renderer = SDL_CreateRenderer(window, -1, 0);
	if (renderer) {
		SDL_SetRenderDrawColor(renderer, 0, 255, 0, 50);
	}
	// Associate an SDL surface with the program window
	screenSurface = SDL_GetWindowSurface(window);
	// Initialize a menu object
    options = new GameOptions();
	gameStage = new mainMenu(window, screenSurface, renderer, options);

	// Start the program loop
	mainloop();
}

/**
Destructor
*/
GameManager::~GameManager() {
    SDL_DestroyWindow(window);
    SDL_DestroyRenderer(renderer);
    delete gameStage;
    delete options;
}

Uint32 ObjectRender(Uint32 interval, void *p)
{
	GameStage *stage = reinterpret_cast<GameStage *>(p);
	stage->render();
	return interval;
}

/**
Program loop.Runs at an undetermined frequency
*/
void GameManager::mainloop()
{
	timer_id = SDL_AddTimer(delay, ObjectRender, gameStage);
	while (run_program) {
		KeyPress pressed = gameStage->getUserInput();
		
		handleInput(pressed);
        gameStage->update();

		SDL_Delay(1);
	}

	SDL_RemoveTimer(timer_id);
    
}

/**
 Handles input from user. First attempts to use the handleInput functio of the GameStage object, then attempts to match them against GameManager actions.
*/
void GameManager::handleInput(KeyPress input)
{
	if (gameStage->handleInput(input)) {
	
	} else {
	switch (input) {
	case FULL_QUIT:
		run_program = false;
		break;
	case MENU_G:
        SDL_RemoveTimer(timer_id);

		delete gameStage;
		gameStage = new Game(window, screenSurface, renderer, options);

		timer_id = SDL_AddTimer(delay, ObjectRender, gameStage);
		break;
	case MENU_P:
        SDL_RemoveTimer(timer_id);
		delete gameStage;
		gameStage = new PlaneMenu(window, screenSurface, renderer, options);
		timer_id = SDL_AddTimer(delay, ObjectRender, gameStage);
		break;
    case MENU_H:
        SDL_RemoveTimer(timer_id);
        delete gameStage;
        gameStage = new HighscoreMenu(window, screenSurface, renderer);
        timer_id = SDL_AddTimer(delay, ObjectRender, gameStage);
        break;
	case GAME_Q:
        SDL_RemoveTimer(timer_id);
		delete gameStage;
		gameStage = new mainMenu(window, screenSurface, renderer, options);
		timer_id = SDL_AddTimer(delay, ObjectRender, gameStage);
		break;
    case HSCORE_H:
        SDL_RemoveTimer(timer_id);
        delete gameStage;
        gameStage = new mainMenu(window, screenSurface, renderer, options);

        timer_id = SDL_AddTimer(delay, ObjectRender, gameStage);
        break;
	case PLANE_Q:
        SDL_RemoveTimer(timer_id);

		delete gameStage;
		gameStage = new mainMenu(window, screenSurface, renderer, options);
		timer_id = SDL_AddTimer(delay, ObjectRender, gameStage);

		break;

	default:
		break;
	}
	}
}

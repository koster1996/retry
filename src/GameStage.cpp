#include "GameStage.h"
#include "GameManager.h"
/**
Abstract class, used by gameManager to use either Game, or MenuPage. Has no data stored, and only virtual functions
*/
GameStage::GameStage() {}

///
/// Virtual function for rendering
void GameStage::render() const {}

///
/// Virtual function for getting input from user
KeyPress GameStage::getUserInput() { return KEY_PRESS_DEFAULT; }

///
/// Virtual function for handling input
bool GameStage::handleInput(KeyPress) { return false; }

///
/// Virtual function for updating the rendering
void GameStage::update() {}

#include "MainMenu.h"
#include "GameManager.h"
#include "GameStage.h"
#include <iostream>


///
/// Constructor for the mainMenu object
mainMenu::mainMenu(SDL_Window *manager_window,
                   SDL_Surface *manager_screenSurface,
                   SDL_Renderer *manager_renderer, GameOptions *options)
    : MenuPage(manager_window, manager_screenSurface, manager_renderer),
      texturePath("./resources/menu.bmp"), gameOptions(options)
{
    menuPageTexture = loadTextures(texturePath);
}

/**
Handles user keyboard input to play the game, quit the program, set level, and
set level difficulty. Returns the enum shared for all key inputs.
@return GameOptions pointer
*/
enum KeyPress mainMenu::getUserInput()
{
    if (SDL_PollEvent(&e)) {
        switch (e.type) {
        case SDL_KEYDOWN:
            switch (e.key.keysym.sym) {
            case SDLK_g:
                return MENU_G;
                break;
            case SDLK_q:
                return FULL_QUIT;
                break;
            case SDLK_o:
                return MENU_O;
            case SDLK_1:
                return MENU_1;
                break;
            case SDLK_2:
                return MENU_2;
                break;
            case SDLK_3:
                return MENU_3;
                break;
            case SDLK_h:
                return MENU_H;
                break;
            case SDLK_p:
                return MENU_P;
                break;
            default:
                return KEY_PRESS_DEFAULT;
                break;
            }
            break;
        case SDL_QUIT:
            return FULL_QUIT;
            break;
        default:
            return KEY_PRESS_DEFAULT;
            break;
        }
    } else {
        return KEY_PRESS_DEFAULT;
    }
}

///
/// Based on input, changes the options config to reflect which level needs to be played
bool mainMenu::handleInput(KeyPress input)
{
    switch (input) {
    case MENU_1:
        gameOptions->setLevel(GAME_LEVEL::GAME_LEVEL_ONE);
        return false;
        break;
    case MENU_2:
        gameOptions->setLevel(GAME_LEVEL::GAME_LEVEL_TWO);
        return true;
        break;
    case MENU_3: 
		gameOptions->setLevel(GAME_LEVEL::GAME_LEVEL_THREE);
        return true;
    default:
        return false;
        break;
    }

}


#include "HighscoreMenu.h"
#include "GameManager.h"
#include "GameStage.h"
#include <iostream>

///
/// Highscore menu constructor which sets the correct texturePath and loads textures.
HighscoreMenu::HighscoreMenu(SDL_Window *manager_window,
                   SDL_Surface *manager_screenSurface,
                             SDL_Renderer *manager_renderer)
    : MenuPage(manager_window, manager_screenSurface, manager_renderer),
      texturePath("./resources/highscoresMenu.bmp")
{
   menuPageTexture = loadTextures(texturePath);
}

/// Handles use input (overridden. Function creates an highscores object and prints
/// the stored highscores by calling showHighscores() on P key.
bool HighscoreMenu::handleInput(KeyPress input)
{
	
    HighScores highscore; /// Create highscore object (constructor loads from text file)
    switch (input) {
    case HSCORE_Q:
        return false;
        break;
    case HSCORE_P:
        highscore.showHighscores(); /// print scores
        return true;
        break;
    default:
        return false;
        break;
    }
}


/**
Gets user input 
*/
enum KeyPress HighscoreMenu::getUserInput()
{
    if (SDL_PollEvent(&e)) {
        switch (e.type) {
        case SDL_KEYDOWN:
            switch (e.key.keysym.sym) {
            case SDLK_h:
                return HSCORE_H;
                break;
            case SDLK_p:
                return HSCORE_P;
                break;
            default:
                return KEY_PRESS_DEFAULT;
                break;
            }
            break;
        case SDL_QUIT:
            return FULL_QUIT;
            break;
        default:
            return KEY_PRESS_DEFAULT;
            break;
        }
	}
	else
	{
        return KEY_PRESS_DEFAULT;
	}
}

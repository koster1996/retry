#include "PlaneMenu.h"
#include "GameManager.h"
#include "GameStage.h"
#include <iostream>

/**
Plane menu constructor initializing the window
*/
PlaneMenu::PlaneMenu(SDL_Window *manager_window,
                     SDL_Surface *manager_screenSurface,
                     SDL_Renderer *manager_renderer, GameOptions *options)
    : MenuPage(manager_window, manager_screenSurface, manager_renderer),
      texturePath("./resources/planeMenu.bmp"), gameOptions(options)
{
    menuPageTexture = loadTextures(texturePath);
    hasChosen = false;
}

///
/// Method for setting plane type based on input
bool PlaneMenu::handleInput(KeyPress input)
{
    switch (input) {
    case KEY_PRESS_DEFAULT:
        return true;
        break;
    case PLANE_1:
        gameOptions->setPlaneType(PLANE_TYPE::PLANE_TYPE_1);
        hasChosen = true;
        return true;
        break;
    case PLANE_2:
        gameOptions->setPlaneType(PLANE_TYPE::PLANE_TYPE_2);
        hasChosen = true;
        return true;
        break;
    case PLANE_3:
        gameOptions->setPlaneType(PLANE_TYPE::PLANE_TYPE_3);
        hasChosen = true;
        return true;
        break;
    default:
        return false;
        break;
    }
}

/**
Gets user input
*/
enum KeyPress PlaneMenu::getUserInput()
{
    if (hasChosen) {
        return PLANE_Q;
    } else {

		if (SDL_PollEvent(&e)) {
			switch (e.type) {
			case SDL_KEYDOWN:
            switch (e.key.keysym.sym) {
            case SDLK_q:
                return PLANE_Q;
					break;
				case SDLK_1:
				 return PLANE_1;
					break;
				case SDLK_2:
					return PLANE_2;
					break;
				case SDLK_3:
					return PLANE_3;
					break;
				default:
					return KEY_PRESS_DEFAULT;
					break;
				}
				break;
			case SDL_QUIT:
				return FULL_QUIT;
				break;
			default:
				return KEY_PRESS_DEFAULT;
				break;
			}
		} else {
		    return KEY_PRESS_DEFAULT;
		}
    }
}

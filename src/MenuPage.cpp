#include "MenuPage.h"
#include "GameManager.h"
#include "GameStage.h"
#include <iostream>

///
/// Constructor for MenuPage
MenuPage::MenuPage(SDL_Window *manager_window,
                   SDL_Surface *manager_screenSurface,
                   SDL_Renderer *manager_renderer)
    : window(manager_window),
      screenSurface(manager_screenSurface), renderer(manager_renderer), texturePath("./resources/menu.bmp")
{
}

///
/// Destructor for MenuPage
MenuPage::~MenuPage() {
	SDL_DestroyTexture(menuPageTexture);
}

///
/// Renders the menu on screen
void MenuPage::render() const {
    SDL_RenderClear(renderer);
    SDL_RenderCopy(renderer, menuPageTexture, NULL, NULL);
    SDL_RenderPresent(renderer);
}

///
/// Loads textures necessary for rendering the menu on screen
SDL_Texture *MenuPage::loadTextures(std::string path)
{
    SDL_Surface *tmpSurface = SDL_LoadBMP(path.c_str());
    if (tmpSurface == nullptr) {
        std::cerr << SDL_GetError() << std::endl;
    }
    SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, tmpSurface);
    if (texture == nullptr) {
        std::cerr << SDL_GetError() << std::endl;
    }
    SDL_FreeSurface(tmpSurface);

    return texture;
}

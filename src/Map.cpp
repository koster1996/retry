#include "Map.h"
#include <iostream>

/**
Constructor, responsible for creating all objects to be rendered within the game
*/
Map::Map(GameOptions *options)
{

    this->setLevel(options);

    this->makeLevelObjects(level, options);
}

///
/// Sets the level based on the config in options
void Map::setLevel(GameOptions *options)
{
    switch (options->getLevel()) {
    case GAME_LEVEL_ONE:
        level = {{
			#include "Level1.def"
        }};
        std::cout << "Level 1" << std::endl;
        break;

    case GAME_LEVEL_TWO:
        level = {{

				#include "Level2.def"
			}};
        std::cout << "Level 2" << std::endl;
        break;

    case GAME_LEVEL_THREE:
        level = {{
			#include "Level3.def"
        }};
        std::cout << "Level 3" << std::endl;
        break;
    }
}

///
/// Adds an object of type Plane into the objectsVector
void Map::createPlaneObject(std::vector<double> xyposition, double height,
                            double length, double orientation, PLANE_TYPE type)
{
    objectsVector.push_back(std::shared_ptr<GameObject>(
        new Plane(xyposition, height, length, orientation, PLANE, type)));
}

///
/// Adds an object of type Coin into the objectsVector
void Map::createCoinObject(std::vector<double> xyposition, double height,
                           double length, double orientation)
{
    objectsVector.push_back(std::shared_ptr<GameObject>(
        new Coin(xyposition, height, length, orientation, COIN, 15)));
}

///
/// Called by the map contructor. Creates all the required GameObjects
void Map::makeLevelObjects(std::vector<std::vector<int>> &level,
                           GameOptions *options)
{
    MAP_WIDTH = 1280;
    MAP_HEIGHT = 960;
    std::vector<double> xypos;

    // Loop through the 2D vector comprising the level
    for (size_t i = 0; i < level.size(); i++) {
        for (size_t j = 0; j < level[i].size(); j++) {

            // Compute xy-position on the screen
            xypos = {static_cast<double>(j) * TILESIZE,
                     static_cast<double>(i) * TILESIZE};

            // Create a plane object
            if (level[i][j] == PLANE_NUM) {
                std::vector<double> xyPlanePos = {
                    static_cast<double>(MAP_WIDTH) / 4,
                    static_cast<double>(MAP_HEIGHT) / 2};

				PLANE_TYPE type = options->getPlaneType();
                Map::createPlaneObject(xyPlanePos, 30, 30, 30,
                                       type);

				switch (type)
				{
                case PLANE_TYPE_1:
                    ss_number = 2;
                    break;
                case PLANE_TYPE_2:
                    ss_number = 7;
                    break;
                case PLANE_TYPE_3:
                    ss_number = 8;
                    break;
				}

                level[i][j] = BACKGROUND_NUM;
            }

            // Create a coin objects
            if (level[i][j] == COIN_NUM) {
                Map::createCoinObject(xypos, OBJECTSIZE, OBJECTSIZE, 0.0);
                level[i][j] = BACKGROUND_NUM;
            }

            // Create any other possible objects
            else {
                for (unsigned int k = 0; k < objectNumbers.size(); k++) {
                    if (level[i][j] == objectNumbers[k]) {
                        Map::createObject(xypos, TILESIZE, TILESIZE, 0, objectTypes[k]);
                        level[i][j] = BACKGROUND_NUM;
                    }
                }
            }
        }
    }
}

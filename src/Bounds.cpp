#define M_PI1 3.141592653589793238462643383279502884L /* pi */
#include "GameObject.h"
#include "math.h"
#include <iostream>
#include <vector>

///
/// Uses same constructor as GameObject
Bounds::Bounds(const std::vector<double> pos, const double worientation,
               const double l, const double h, const Type ttype)
    : GameObject(pos, worientation, l, h, ttype)
{
}

///
/// Overrides the has collided function in GameObject with a height checker
/// instead.
bool Bounds::hasCollided(std::shared_ptr<GameObject> other_object) /*const*/
{

    bool hasCollided;

    std::vector<double> height_bounds = {-270, 270};

    if (other_object->getPosition().at(1) > height_bounds.at(0) &&
        (other_object->getPosition().at(1) < height_bounds.at(1))) {
        hasCollided = false;
    } else {
        hasCollided = true;
    }

    return hasCollided;
}
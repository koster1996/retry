#include "Options.h"

///
/// Constructor for GameOptions. Initializing with the default game settings.
GameOptions::GameOptions()
{
    level = GAME_LEVEL::GAME_LEVEL_ONE;
    difficulty = GAME_DIFFICULTY::GAME_DIFFICULTY_MED;
    plane = PLANE_TYPE::PLANE_TYPE_1;
}

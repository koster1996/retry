/// \file Retry project main file
/// main.cpp

#include <SDL2/SDL.h> 
// Retry project
// Main file
#include "GameObject.h"
#include "MainMenu.h"
#include "Game.h"
#include "GameManager.h"
#include <vector>
#include <iostream>

///
/// Program entry point.
int main()							//comment this line if compiling on windows
//int main(int argc,char *args[]) // SDL apparently requires char* array as second argument. Uncomment this line if compiling on windows.
{
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
        printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
    } else { 
		
		GameManager TheManager;
	}

    return 0;
}


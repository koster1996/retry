#include "Game.h"
#include "GameManager.h"
#include "GameObject.h"
#include "Highscores.h"
#include "Map.h"
#include "math.h"
#include <iostream>

/// Constructor for the game class.
/// Takes in a SDL window, SDL surface, and a SDL Renderer
/// Later iterations of this manager will also load the map upon initialization,
/// and all the objects

Game::Game(SDL_Window *manager_window, SDL_Surface *manager_screenSurface,
           SDL_Renderer *manager_renderer, GameOptions *options)
{
    window = manager_window;
    screenSurface = manager_screenSurface;
    renderer = manager_renderer;

    SDL_SetRenderDrawColor(renderer, 0, 0, 50, 255);

    last_press_time = SDL_GetTicks() / 1000.0;
    time = SDL_GetTicks() / 1000.0 - last_press_time;
    delta_t = 0.01;
    clock = SDL_GetTicks() / 1000.0;
    start_of_game = SDL_GetTicks() / 1000.0;

    this->map = new Map(options);
    this->level = map->getLevel();
    this->objectNumbers = map->getObjectNumbers();
    this->objectTypes = map->getObjectTypes();
    this->GameObjects = map->getMapObjects();
    this->plane_ss_number = map->getPlaneSSNumber();

    score = 0;

    loadTextures();

    isDead = false;
}

///
/// Deconstructor for Game, deletes all the textures used by Game
Game::~Game()
{
    SDL_DestroyTexture(textures);
    delete map;
}
///
/// Handles input of the user, and will also handle the case that the game has ended.
enum KeyPress Game::getUserInput()
{
    if (SDL_PollEvent(&e)) {
        switch (e.type) {
        case SDL_KEYDOWN:
            switch (e.key.keysym.sym) {
            case SDLK_SPACE:
                return GAME_UP;
                break;
            case SDLK_q:
                return GAME_Q;
                break;
            default:
                return KEY_PRESS_DEFAULT;
                break;
            }
            break;
        case SDL_QUIT:
            return FULL_QUIT;
            break;
        default:
            return KEY_PRESS_DEFAULT;
            break;
        }
    } else {
        if (isDead) {
            return GAME_Q;
        } else {
            return KEY_PRESS_DEFAULT;
        }
    }
}

///
/// Handle the input, and convert it to actions
bool Game::handleInput(KeyPress input)
{
    keyPressed = false;
    switch (input) {
    case GAME_UP:
        last_press_time = SDL_GetTicks() / 1000.0;
        keyPressed = true;
        return true;
        break;
    default:
        return false;
        break;
    }
}
///
/// Updates every object with a new position, based on the user input and the
/// time since last keypress
void Game::update()
{
    HighScores highscore;
    if (!isDead) {
        delta_t = SDL_GetTicks() / 1000.0 - clock;
        clock = SDL_GetTicks() / 1000.0;
        time = SDL_GetTicks() / 1000.0 - last_press_time;
        for (auto &item : GameObjects) {
            item->updatePosition(time, keyPressed, delta_t);
            if (item->getType() == PLANE) {
                // checking if the plane has collided with any other objects
                for (auto &object : GameObjects) {
                    if (object->getType() != PLANE) {
                        if (item->hasCollided(object)) {
                            if (object->getType() == COIN) {
                                if (!object->isDead()) {
                                    score = score + object->getValue();
                                    object->setDeath(true);
                                }
                            } else {
                                item->setDeath(true);
                                score = score + (int)(clock - start_of_game);
                                if (object->getType() == FINISHLINE) {
                                    std::cout << "congratulations, you have finished the level!" << std::endl;
                                    score = score + 50;
								}

                                highscore.newEntry((double)score);
                            }
                        }
                    }
                }
                isDead = item->isDead();
            }
        }
    }
}
///
/// Draws the objects to screen. Uses a right-handed coordinate frame, because of
/// a inverted y-axis
void Game::render() const
{
    SDL_RenderClear(renderer);

    // Create a source rectangle for the spritesheet, x-position varies
    SDL_Rect src;
    src.y = 0;
    src.w = 128;
    src.h = 128;

    // Render the background
    SDL_Rect SDL_Rect_Background = {0, 0, 1280, 960};
    src.x = BACKGROUND_NUM * 128;
    SDL_RenderCopy(renderer, textures, &src, &SDL_Rect_Background);

    // Loop through and render GameObjects
    for (auto &item : GameObjects) {
        // Construct the destination rectangle
        SDL_Rect SDL_Rectangle = {
            (int)item->getPosition().at(0), (int)item->getPosition().at(1),
            (int)item->getLength(), (int)item->getHeight()};

        // Obtain the type of the GameObject
        Type type = item->getType();

        // Check if destination rectangle is within the screen
        if (SDL_Rectangle.x >= -TILESIZE && SDL_Rectangle.x <= 1280) {
            if (SDL_Rectangle.y >= -TILESIZE && SDL_Rectangle.y <= 960) {
                // Render the plane
                if (type == PLANE) {
                    src.x = plane_ss_number * 128;
                    SDL_RenderCopyEx(renderer, textures, &src, &SDL_Rectangle,
                                     -1.0 * item->getOrientation() * 360 /
                                         (2 * 3.1415),
                                     0, SDL_FLIP_NONE);
                }

                // Render the coin
                if (type == COIN) {
                    if (!item->isDead()) {
                        src.x = COIN_NUM * 128;
                        SDL_RenderCopy(renderer, textures, &src,
                                       &SDL_Rectangle);
                    }
                }

                // Render all remaining objects
                else {
                    for (unsigned int i = 0; i < objectNumbers.size(); i++) {
                        if ((type == objectTypes[i]) && (type != PLANE) &&
                            (type != COIN)) {
                            src.x = objectNumbers[i] * 128;
                            SDL_RenderCopy(renderer, textures, &src,
                                           &SDL_Rectangle);
                        }
                    }
                }
            }
        };
    };

    SDL_RenderPresent(renderer);
}

/// Loads the texture from file. Currently there is only a single texture for
/// all objects
void Game::loadTextures()
{

    SDL_Surface *surface = SDL_LoadBMP("resources/retry_ss.bmp");

    if (surface == nullptr) {
        std::cerr << SDL_GetError() << std::endl;
    }

    this->textures = SDL_CreateTextureFromSurface(renderer, surface);

    SDL_FreeSurface(surface);
}